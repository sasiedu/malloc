/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/13 06:15:37 by sasiedu           #+#    #+#             */
/*   Updated: 2017/06/13 07:49:28 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

int		main(void)
{
	char* a = malloc(120);
	printf("+++addr of a: %p\n", a);
	char* b = realloc(a, 50);
	printf("+++addr of b: %p\n", b);
	free(b);
//	char* c = realloc(a, 200);
	char* d = realloc(NULL, 90);
	printf("+++addr of d: %p\n", d);
	char* e = malloc(90);
	printf("+++addr of e: %p\n", e);
	char* f = realloc(e, 0);
	printf("+++addr of f: %p\n", f);
	ft_memcpy(d, "this is good", 12);
	char* g = realloc(d, N + 1);
	printf("+++addr of g: %p\n", g);
	printf("value in g: %s\n", g);

	printf("\n\n\n");
	char* aa = malloc(N + 1);
	ft_memcpy(aa, "idjemldnshyeiwoplfkm", 20);
	printf("+++addr of aa: %p\n", aa);
	printf("value of aa: %s\n", aa);
	char* bb = realloc(aa, 10);
	printf("+++addr of bb: %p\n", bb);
	printf("value of bb: %s\n", bb);
	char* cc = realloc(bb, M + 1);
	printf("+++addr of cc: %p\n", cc);

	printf("\n\n\n");
	char* aaa = malloc(M + 10);
	printf("+++addr of aaa: %p\n", aaa);
	char* bbb = realloc(aaa, M + 5);
	printf("+++addr of bbb: %p\n", bbb);
	char* ccc = realloc(bbb, 50);
	printf("+++addr of ccc: %p\n", ccc);

	char* m = malloc(50);
	char* n = malloc(N - 5);
	char* o = malloc(N + 20);
	char* p = malloc(M - 50);
	char* q = malloc(M + 25);
	char* r = malloc(M + 250);
	printf("\n\n\n");
	show_alloc_mem();
	free(m);
	free(n);
	free(o);
	free(p);
	free(q);
	free(r);
	return 0;
}
