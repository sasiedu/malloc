/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_large.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/09 03:37:11 by sasiedu           #+#    #+#             */
/*   Updated: 2017/06/14 08:30:42 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void		destroy_block(t_block **block)
{
	munmap((*block)->memory, (*block)->size);
	(*block)->memory = NULL;
	munmap(*block, sizeof(t_block));
	*block = NULL;
}

void		free_full_block(t_block *block)
{
	if (block == g_ram->large->first_block)
	{
		g_ram->large->blocks = block->next;
		if (g_ram->large->blocks != NULL)
			g_ram->large->blocks->prev = NULL;
		destroy_block(&g_ram->large->first_block);
		g_ram->large->first_block = g_ram->large->blocks;
	}
	else
	{
		block->prev->next = block->next;
		if (block->next != NULL)
			block->next->prev = block->prev;
		destroy_block(&block);
	}
	g_ram->large->free_blocks -= 1;
}

void		throw_malloc_error(void *memory, char *proc)
{
	pthread_t			current_thread;

	current_thread = pthread_self();
	ft_putstr("malloc: *** error for object 0x");
	ft_putnbr_base((long)memory, 16);
	ft_putstr(": pointer being ");
	ft_putstr(proc);
	ft_putstr(" was not allocated\n");
	ft_putstr(" *** set a breakpoint in malloc_error_break to debug\n");
	pthread_kill(current_thread, 6);
}

void		free_large_block(t_block *block, void *memory)
{
	(void)memory;
	free_full_block(block);
	g_ram->large->free_blocks -= 1;
}
