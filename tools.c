/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/12 14:11:39 by sasiedu           #+#    #+#             */
/*   Updated: 2017/06/14 08:01:48 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

size_t	ft_strlen(const char *s)
{
	size_t i;

	i = 0;
	if (s == NULL)
		return (0);
	while (s[i] != '\0')
		i++;
	return (i);
}

void	ft_putstr(char const *s)
{
	write(1, s, ft_strlen(s));
}

void	ft_putnbr_base(long int nbr, int base)
{
	static	char	*str = "0123456789abcdefghijklmnopqrstuvwxyz";

	if (nbr < 0 && base == 10)
		write(1, "-", 1);
	if (nbr < 0)
		nbr *= -1;
	if (nbr >= base)
		ft_putnbr_base(nbr / base, base);
	write(1, &str[nbr % base], 1);
}
