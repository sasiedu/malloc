# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/06/06 06:03:46 by sasiedu           #+#    #+#              #
#    Updated: 2017/06/13 07:41:53 by sasiedu          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

ifeq ($(HOSTTYPE),)
	HOSTTYPE := $(shell uname -m)_$(shell uname -s)
endif

NAME = "libft_malloc_$(HOSTTYPE).so"

CC = gcc

CFLAGS = -Wall -Wextra -Werror

RM = rm -f

SRCS = malloc.c realloc.c free.c zone.c block.c free_large.c \
		free_tiny_small.c tools.c tools2.c print.c

OBJS = $(SRCS:.c=.o)

all: ${NAME}

$(NAME): $(OBJS)
		@echo "compiling library\033[0m"
		$(CC) -shared -o $(NAME) $(OBJS) -lpthread
		@echo "library successfully compiled\033[0m"
		@echo "linking libraay\033[0m"
		ln -s $(NAME) libft_malloc.so
		@echo "library successfully linked\033[0m"

%.o : %.c
	$(CC) -c $(CFLAGS) -I . $<

clean:
	-${RM} ${OBJS} $(SRCS:.c=.d)

fclean: clean
	-${RM} ${NAME}
	-${RM} libft_malloc.so

re: fclean all
