/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   block.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/07 11:19:06 by sasiedu           #+#    #+#             */
/*   Updated: 2017/06/14 08:28:26 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void		*get_memory(void *ptr, size_t size)
{
	return (mmap(ptr, size, PROT_READ | PROT_WRITE,
			MAP_PRIVATE | MAP_ANON, -1, 0));
}

t_block		*new_block(void *memory, size_t size)
{
	t_block		*new;

	new = (t_block *)mmap(NULL, sizeof(t_block), PROT_READ | PROT_WRITE,
			MAP_PRIVATE | MAP_ANON, -1, 0);
	if (new == MAP_FAILED)
		return (NULL);
	new->empty = 0;
	new->size = size;
	new->memory = memory;
	new->first = NULL;
	new->prev = NULL;
	new->next = NULL;
	return (new);
}

void		add_block(t_block **blocks, t_block *first, t_block *new)
{
	t_block		*tmp;
	t_block		*prev;

	tmp = *blocks;
	prev = NULL;
	new->first = first;
	while (tmp != NULL)
	{
		prev = tmp;
		tmp = tmp->next;
	}
	if (prev != NULL)
	{
		prev->next = new;
		new->prev = prev;
	}
	else
		*blocks = new;
}

t_block		*get_empty(t_block *blocks)
{
	t_block		*tmp;

	tmp = (blocks != NULL) ? blocks->first : blocks;
	while (tmp != NULL)
	{
		if (tmp->empty == 1)
			break ;
		tmp = tmp->next;
	}
	return (tmp);
}
