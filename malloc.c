/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/06 06:02:13 by sasiedu           #+#    #+#             */
/*   Updated: 2017/06/14 08:16:46 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

t_alloc		*g_ram = NULL;

void		init_ram(void)
{
	if (g_ram == NULL)
	{
		g_ram = mmap(NULL, sizeof(t_alloc), PROT_READ | PROT_WRITE,
				MAP_PRIVATE | MAP_ANON, -1, 0);
		if (g_ram == MAP_FAILED)
		{
			g_ram = NULL;
			return ;
		}
		g_ram->tiny = new_zone(N_ZONE);
		g_ram->small = new_zone(M_ZONE);
		g_ram->large = new_zone(0);
		g_ram->tiny->type = TINY;
		g_ram->small->type = SMALL;
		g_ram->large->type = LARGE;
	}
}

void		*malloc(size_t size)
{
	t_block		*block;

	if (size < 1)
		return (NULL);
	if (g_ram == NULL)
		init_ram();
	if (g_ram == NULL)
		return (NULL);
	if (size <= (size_t)N)
		block = get_tiny_or_small_memory(g_ram->tiny);
	else if (size <= (size_t)M)
		block = get_tiny_or_small_memory(g_ram->small);
	else
		block = get_large_memory(g_ram->large, size);
	if (block != NULL)
	{
		block->empty = 1;
		block->ask_size = size;
		return (block->memory);
	}
	return (NULL);
}
