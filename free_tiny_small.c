/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_tiny_small.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/12 11:09:27 by sasiedu           #+#    #+#             */
/*   Updated: 2017/06/14 08:54:16 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void		free_full_block2(t_block *block)
{
	if (!block->empty)
		throw_malloc_error(block->memory, "free");
	block->empty = 0;
}

void		free_tiny_or_small_block(t_block *block, int type)
{
	free_full_block2(block);
	if (type == TINY)
		g_ram->tiny->free_blocks += 1;
	else
		g_ram->tiny->free_blocks += 1;
}
