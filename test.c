/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/06 06:49:18 by sasiedu           #+#    #+#             */
/*   Updated: 2017/06/07 10:49:43 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/mman.h>
#include <string.h>
#include <sys/time.h>
#include <sys/resource.h>

typedef struct s_data
{
	int			num;
	char		*data;
	struct s_data	*next;
}				t_data;

static t_data*	test = NULL;

void		is_same_address(void*	ptr)
{
	void* tmp = (void *)test;
	if (tmp == ptr)
		printf("tmp is same\n");
	else
		printf("tmp is not same\n");
	if (test == ptr)
		printf("test is same\n");
	else
		printf("test is not same\n");
}

void	test_free(void* ptr)
{
	printf("%p\n", ptr);
	printf("%p\n", test);
	printf("%s\n", (char *)ptr);
	ptr = malloc(20);
}

int main(void)
{
	printf("page size %d\n", getpagesize());
	/*char*	ptr = malloc(sizeof(char) * 10);
	printf("%p\n", ptr);
	free(ptr);
	printf("%p\n", ptr);*/

	/*char* ptr = mmap(NULL, getpagesize(), PROT_READ | PROT_WRITE, 
			MAP_PRIVATE | MAP_ANON, -1, 0);
	printf("%p\n", ptr);
	printf("%p\n", ++ptr);
	printf("%p\n", ++ptr);
	printf("%p\n", ++ptr);
	ptr[0] = 'a';
	printf("%c\n", ptr[0]);
	munmap(ptr, getpagesize());
	printf("%p\n", ptr);
	printf("%c\n", ptr[0]);*/
	test = (t_data*) mmap(NULL, sizeof(t_data), PROT_READ | PROT_WRITE,
			MAP_PRIVATE | MAP_ANON, -1, 0);
	test->num = 1;
	is_same_address((void*)test);
	printf("%d\n", test->num);
	printf("%zu\n", sizeof(*test));
	//free(test);
	//is_same_address((void*)test);
	//memset((void *)test, '\0', sizeof(*test));
	//munmap(test, sizeof(*test));
	//test_free(test);
	//printf("%d\n", test->num);
	/*struct rlimit *rlim = malloc(sizeof(struct rlimit));
	struct rlimit *rlim2 = malloc(sizeof(struct rlimit));
	getrlimit(RLIMIT_AS, rlim);
	printf("soft: %llu\n", rlim->rlim_cur);
	printf("hard: %llu\n", rlim->rlim_max);
	getrlimit(RLIMIT_MEMLOCK, rlim2);
	printf("soft: %llu\n", rlim2->rlim_cur / 1000000000000);
	printf("hard: %llu\n", rlim2->rlim_max / 1000000000000);
	free(rlim);
	free(rlim2);*/
	return 0;
}
