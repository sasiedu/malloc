/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/06 06:02:06 by sasiedu           #+#    #+#             */
/*   Updated: 2017/06/14 08:58:49 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MALLOC_H
# define MALLOC_H

# include <sys/mman.h>
# include <unistd.h>
# include <sys/time.h>
# include <sys/resource.h>
# include <unistd.h>
# include <pthread.h>

# define N (2 * getpagesize())
# define N_ZONE (N * 100)
# define M (4 * getpagesize())
# define M_ZONE (M * 100)
# define TINY (1)
# define SMALL (2)
# define LARGE (3)

typedef struct			s_block
{
	int					empty;
	size_t				size;
	size_t				ask_size;
	void				*memory;
	struct s_block		*first;
	struct s_block		*prev;
	struct s_block		*next;
}						t_block;

typedef struct			s_zone
{
	int					type;
	int					free_blocks;
	size_t				size;
	t_block				*first_block;
	t_block				*blocks;
}						t_zone;

typedef struct			s_alloc
{
	t_zone				*tiny;
	t_zone				*small;
	t_zone				*large;
	rlim_t				soft_limit;
	rlim_t				hard_limit;
}						t_alloc;

extern	t_alloc			*g_ram;

size_t					ft_strlen(const char *s);
void					ft_putstr(char const *s);
void					ft_putnbr_base(long int nbr, int base);
void					*ft_memcpy(void *restrict dst, \
							const void *restrict src, size_t n);

void					*get_memory(void *ptr, size_t size);
t_block					*new_block(void *memory, size_t size);
void					add_block(t_block **blocks, t_block *first, \
							t_block *new);
t_block					*get_empty(t_block *blocks);

t_zone					*new_zone(size_t size);
t_block					*init_zone_blocks(t_zone *zone, int *count, int size);
t_block					*get_tiny_or_small_memory(t_zone *zone);
t_block					*get_large_memory(t_zone *zone, size_t size);

void					init_ram(void);
void					*malloc(size_t size);

t_block					*find_memory_block(t_block *blocks, void *memory);
void					free(void *memory);
t_zone					*get_memory_zone(void *memory);
t_block					*get_memory_block(t_block *blocks, void *memory);
void					free_large_block(t_block *block, void *memory);
void					free_tiny_or_small_block(t_block *block, int type);
void					throw_malloc_error(void *memory, char *proc);

void					*realloc(void *ptr, size_t size);

void					show_alloc_mem(void);

#endif
