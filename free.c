/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/06 06:03:33 by sasiedu           #+#    #+#             */
/*   Updated: 2017/06/14 08:27:39 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

t_zone			*get_memory_zone(void *memory)
{
	t_zone		*zone;

	zone = g_ram->large;
	if (g_ram->tiny->first_block != NULL &&
			(long)memory >= (long)g_ram->tiny->first_block->memory &&
			(long)memory <= (long)g_ram->tiny->first_block->memory + N_ZONE)
		zone = g_ram->tiny;
	else if (g_ram->small->first_block != NULL &&
			(long)memory >= (long)g_ram->small->first_block->memory &&
			(long)memory <= (long)g_ram->small->first_block->memory + M_ZONE)
		zone = g_ram->small;
	return (zone);
}

t_block			*get_memory_block(t_block *blocks, void *memory)
{
	t_block		*tmp;

	tmp = blocks;
	while (tmp != NULL)
	{
		if ((long)memory == (long)tmp->memory)
			break ;
		tmp = tmp->next;
	}
	return (tmp);
}

void			free(void *memory)
{
	t_block		*block;
	t_zone		*zone;

	if (g_ram == NULL)
		return (throw_malloc_error(memory, "freed"));
	zone = get_memory_zone(memory);
	block = get_memory_block(zone->first_block, memory);
	if (block == NULL)
		return (throw_malloc_error(memory, "freed"));
	if (zone->type == LARGE)
		free_large_block(block, memory);
	else
		free_tiny_or_small_block(block, zone->type);
}
