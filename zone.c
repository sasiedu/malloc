/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   zone.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/07 11:19:12 by sasiedu           #+#    #+#             */
/*   Updated: 2017/06/14 08:27:17 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

t_zone			*new_zone(size_t size)
{
	t_zone		*zone;

	zone = (t_zone *)mmap(NULL, sizeof(t_zone) + size, PROT_READ | PROT_WRITE,
				MAP_PRIVATE | MAP_ANON, -1, 0);
	if (zone == MAP_FAILED)
		return (NULL);
	zone->type = -1;
	zone->free_blocks = 0;
	zone->first_block = NULL;
	zone->blocks = NULL;
	zone->size = 0;
	return (zone);
}

t_block			*init_zone_blocks(t_zone *zone, int *count, int size)
{
	void		*memory;
	t_block		*blocks;
	t_block		*first;
	t_block		*new;

	first = NULL;
	blocks = NULL;
	memory = zone + sizeof(t_zone);
	if ((first = new_block(memory, size)) == NULL)
		return (NULL);
	add_block(&blocks, first, first);
	memory = memory + size;
	while (*count < 100)
	{
		if ((new = new_block(memory, size)) == MAP_FAILED)
			break ;
		add_block(&blocks, first, new);
		*count += 1;
		memory = memory + size;
	}
	return (first);
}

t_block			*get_tiny_or_small_memory(t_zone *zone)
{
	t_block		*tmp;

	if (zone->first_block == NULL)
	{
		zone->blocks = init_zone_blocks(zone, &zone->free_blocks,
				(zone->type == TINY) ? N : M);
		zone->first_block = zone->blocks;
	}
	tmp = zone->first_block;
	while (tmp != NULL)
	{
		if (tmp->empty == 0)
			break ;
		tmp = tmp->next;
	}
	zone->free_blocks = (tmp != NULL) ? zone->free_blocks - 1 :
		zone->free_blocks;
	return (tmp);
}

t_block			*get_large_memory(t_zone *zone, size_t size)
{
	void		*memory;
	t_block		*block;

	if ((memory = get_memory(NULL, size)) == MAP_FAILED)
		return (NULL);
	if ((block = new_block(memory, size)) == NULL)
	{
		munmap(memory, size);
		return (NULL);
	}
	if (zone->first_block == NULL)
	{
		add_block(&zone->blocks, block, block);
		zone->first_block = block;
	}
	else
		add_block(&zone->blocks, zone->first_block, block);
	zone->free_blocks += 1;
	zone->size += size;
	return (block);
}
