/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/07 10:50:54 by sasiedu           #+#    #+#             */
/*   Updated: 2017/06/13 06:13:10 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

int		main(void)
{
	printf("M ZONE: %d\n", M_ZONE);
	printf("N ZONE: %d\n", N_ZONE);
	printf("M	  : %d\n", M);
	printf("N	  : %d\n", N);
	int		count = 0;

	/*t_block*	blocks = init_zone_blocks(&count, N);
	printf("count: %d\n", count);
	printf("addr: %p\n", blocks->memory);
	printf("addr: %p\n", blocks->next->memory);
	printf("addr: %p\n", blocks->next->next->memory);
	printf("addr: %p\n", blocks->next->prev->memory);*/

	/*t_zone*	zone1 = new_zone();
	t_zone*	zone2 = new_zone();
	t_zone*	zone3 = new_zone();
	zone1->type = TINY;
	zone2->type = SMALL;
	zone3->type = LARGE;
	
	t_block* b1 = get_tiny_or_small_memory(zone1);
	b1->empty = 1;
	printf("b1: %p\n", b1->memory);
	t_block* b2 = get_tiny_or_small_memory(zone1);
	b2->empty = 1;
	printf("b2: %p\n", b2->memory);
	
	t_block* b3 = get_tiny_or_small_memory(zone2);
	b3->empty = 1;
	printf("b3: %p\n", b3->memory);
	t_block* b4 = get_tiny_or_small_memory(zone2);
	b4->empty = 1;
	printf("b4: %p\n", b4->memory);


	t_block* b5 = get_large_memory(zone3, 10119083);
	b5->empty = 1;
	printf("b5: %p\n", b5->memory);
	t_block* b6 = get_large_memory(zone3, 70879083);
	b6->empty = 1;
	printf("b6: %p\n", b6->memory);
	
	printf("zone1 size: %d\n", zone1->free_blocks);
	printf("zone2 size: %d\n", zone2->free_blocks);
	printf("zone3 size: %d\n", zone3->free_blocks);*/

	char*	p = (char *)malloc(sizeof(char) * 40);
	p[0] = 'd';
	p[1] = 'o';
	p[2] = 'g';
	p[3] = 's';
	printf("p: %s\n", p);
	int*	a = (int *)malloc(sizeof(int) * 50 + N);
	if (a != NULL)
		printf("a size: %zu\n", sizeof(a));
	if (malloc(0) == NULL)
		printf("failed with 0\n");
	struct rlimit *rlim = malloc(sizeof(struct rlimit));
	getrlimit(RLIMIT_AS, rlim);
	//printf("soft: %llu\n", rlim->rlim_cur);
	//printf("hard: %llu\n", rlim->rlim_max);
	size_t t = 5500000000000000000;
	//printf("num: %zu\n", t);
	if (malloc(t) == NULL)
		printf("failed with huge memory size\n");
	char*	c = (char *)malloc(sizeof(char) * M + 150);
	c[0] = 'p';
	c[1] = 'e';
	c[2] = 't';
	c[3] = 'e';
	c[4] = 'r';
	printf("c: %s\n", c);
	char*	cc = (char *)malloc(sizeof(char) * M + 150);
	cc[0] = 'j';
	cc[1] = 'a';
	cc[2] = 'm';
	cc[3] = 'e';
	cc[4] = 's';
	printf("cc: %s\n", cc);
	if (ram == NULL)
		printf("ram is null in main\n");
	//free(p);
	//free(&p[3]);
	//free(a);
	//free(&a[30]);
//	free(c);
	//free(&c[M + 150]);
	printf("cc: %s\n", cc);
	//printf("c: %s\n", c);
	cc[M + 50] = 'c';
	cc[M + 51] = 'a';
	cc[M + 52] = 't';
	printf("partial: %s\n", &cc[M + 50]);
	//free(&cc[M + 50]);
	//free(cc);
	//printf("cc: %s\n", cc);
	//printf("partial: %s\n", &cc[M + 50]);
	printf("addr of p: %p \n", p);
	free(p);
	printf("p: %s\n", p);
	char* d = (char*)malloc(sizeof(char) * 10);
	printf("addr of d: %p\n", d);
	//free(++p);
	//free(d);
	printf("+++++\n");
	char* z;
	//z = realloc((void*)0x01, 10);
	z = realloc(d, 10);
	printf("addr of z: %p\n", z);
	char* u = realloc(d, N + 1);
	printf("addr of u: %p\n", u);
	char* e = realloc(NULL, 120);
	printf("addr of e: %p\n", e);
	free(e);
	char* as = realloc(d, N + 1);
	printf("addr of as: %p\n", as);
	char* at = malloc(50);
	printf("addr of at: %p\n", at);
	return 0;
}
