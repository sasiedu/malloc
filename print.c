/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/13 07:17:19 by sasiedu           #+#    #+#             */
/*   Updated: 2017/06/14 08:18:32 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

size_t			print_block(t_block *b)
{
	ft_putstr("0x");
	ft_putnbr_base((long)b->memory, 16);
	ft_putstr(" - 0x");
	ft_putnbr_base((long)b->memory + b->ask_size, 16);
	ft_putstr(" : ");
	ft_putnbr_base(b->ask_size, 10);
	ft_putstr(" bytes\n");
	return (b->ask_size);
}

size_t			print_zone(char *zone_name, t_zone *zone)
{
	t_block		*block;
	size_t		size;

	size = 0;
	block = zone->first_block;
	ft_putstr(zone_name);
	ft_putstr(" : 0x");
	ft_putnbr_base((long)zone, 16);
	ft_putstr("\n");
	while (block != NULL)
	{
		if (block->empty == 1)
			size += print_block(block);
		block = block->next;
	}
	return (size);
}

void			show_alloc_mem(void)
{
	size_t		size;

	size = 0;
	if (g_ram == NULL)
		init_ram();
	if (g_ram == NULL)
		return ;
	size += print_zone("TINY", g_ram->tiny);
	size += print_zone("SMALL", g_ram->small);
	size += print_zone("LARGE", g_ram->large);
	ft_putstr("Total : ");
	ft_putnbr_base(size, 10);
	ft_putstr(" bytes\n");
}
