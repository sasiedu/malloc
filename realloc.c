/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   realloc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/06 06:29:14 by sasiedu           #+#    #+#             */
/*   Updated: 2017/06/14 08:25:48 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void			*realloc_to_tiny(t_block *block, size_t size)
{
	void		*memory;

	if (block->empty == 0)
		throw_malloc_error(block->memory, "realloc'd");
	if (size <= block->size)
		return (block->memory);
	memory = malloc(size);
	if (memory == NULL)
		return (NULL);
	ft_memcpy(memory, block->memory, size);
	free(block->memory);
	return (memory);
}

void			*realloc_to_small(t_block *block, size_t size)
{
	void		*memory;

	if (block->empty == 0)
		throw_malloc_error(block->memory, "realloc'd");
	if (size > (size_t)N && size <= block->size)
		return (block->memory);
	memory = malloc(size);
	ft_memcpy(memory, block->memory, size);
	free(block->memory);
	return (memory);
}

void			*realloc_to_large(t_block *block, size_t size)
{
	void		*memory;

	if (block->empty == 0)
		throw_malloc_error(block->memory, "realloc'd");
	if (size > (size_t)M && size <= block->size)
		return (block->memory);
	memory = malloc(size);
	ft_memcpy(memory, block->memory, size);
	free(block->memory);
	return (memory);
}

void			*my_realloc(void *ptr, size_t size)
{
	t_zone		*zone;
	t_block		*block;

	(void)zone;
	block = NULL;
	if (size == 0)
	{
		free(ptr);
		return (NULL);
	}
	if (g_ram == NULL)
		throw_malloc_error(ptr, "realloc'd");
	zone = get_memory_zone(ptr);
	if (zone != NULL)
		block = get_memory_block(zone->first_block, ptr);
	if (block == NULL)
		throw_malloc_error(ptr, "realloc'd");
	return (block);
}

void			*realloc(void *ptr, size_t size)
{
	t_block	*block;

	if (ptr == NULL || (long)ptr == 0)
		return (malloc(size));
	block = my_realloc(ptr, size);
	if (block == NULL)
		return (NULL);
	if (block->size == (size_t)N)
		return (realloc_to_tiny(block, size));
	else if (block->size == (size_t)M)
		return (realloc_to_small(block, size));
	else
		return (realloc_to_large(block, size));
	return (block->memory);
}
